import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export class PostService {
  
  constructor(private http: HttpClient) {}

  createPost(formData: FormData): Observable<any> {
    return this.http.post('your-api-endpoint/posts', formData);
  }
  
  
  
  

}
