import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { FollowingComponent } from './following/following.component';


import { AllpostComponent } from './allpost/allpost.component';
import { UserComponent } from './user/user.component';
import { FollowersComponent } from './followers/followers.component';
const routes: Routes = [{
  path: 'login',
  component: LoginComponent},
  // {path:'',redirectTo:'/login',pathMatch:'full'},
  
  { path:'profile',component:ProfileComponent},



 
{
path: 'home',
component: HomeComponent,

},
{

path: 'register',
component: RegisterComponent,
},
{ path: 'followers', component: FollowersComponent },
{ path: 'following', component: FollowingComponent },


{path:'allpost', component:AllpostComponent},
{path:'user', component:UserComponent},

{ path: '', redirectTo: '/news-feed', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


